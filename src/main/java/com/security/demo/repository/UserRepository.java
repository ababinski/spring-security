package com.security.demo.repository;

import com.security.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("SELECT DISTINCT user FROM User user INNER JOIN FETCH user.authorities AS authorities WHERE user.username = :username")
    User findByUsername(@Param("username") String username);
}
