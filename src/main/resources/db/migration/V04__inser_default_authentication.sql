insert into oauth_client_details(client_id, resource_ids, client_secret, scope, authorized_grant_types, authorities, access_token_validity, refresh_token_validity)
 values ('read-client', 'resource-server-rest-api','$2a$04$wgq2p9egioyoofembrfsio9qtcyjtnrnpknbl5tokp7ip.ezn93km',
 'read', 'password,authorization_code,refresh_token,implicit', 'user', 10800, 2592000);

insert into oauth_client_details(client_id, resource_ids, client_secret, scope, authorized_grant_types, authorities, access_token_validity, refresh_token_validity)
 values ('read-write-client', 'resource-server-rest-api','$2a$04$soeor.qfmclxefirhjvlwoqxfhjsjlspwru1igxcmgdu.a5hvfy4w',
 'read,write', 'password,authorization_code,refresh_token,implicit', 'user', 10800, 2592000);