INSERT INTO users(id, user_name, password, account_expired, account_locked, credentials_expired, enabled)
VALUES (1, 'admin','$2a$08$qvrzqz7jj7oy2p/msl4m0.l83cd0jnsx6ajuitbgrxgzge4j035ha', false, false, false, true);

INSERT INTO users(id, user_name, password, account_expired, account_locked, credentials_expired, enabled)
VALUES (2, 'reader', '$2a$08$dwyz8o.qtuxbogosjfss4u19lhkw7acq0lxxunlrfjjgkwj5nfkse', false, false, false, true);

INSERT INTO users(id, user_name, password, account_expired, account_locked, credentials_expired, enabled)
VALUES (3, 'modifier','$2a$08$kpjzxewxrgnriiul4ftqh.mhmn7zafbykb3roz.j24ix8vdacthsg', false, false, false, true);

INSERT INTO users(id, user_name, password, account_expired, account_locked, credentials_expired, enabled)
VALUES (4, 'reader2','$2a$08$vvxqh6s8tqfhms1slntu/.j25iucrpgbpygexa.9yi.ildradr6ea', false, false, false, true);

INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 1);
INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 2);
INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 3);
INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 4);
INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 5);
INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 6);
INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 7);
INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 8);
INSERT INTO users_authorities(user_id, authority_id) VALUES (1, 9);

INSERT INTO users_authorities(user_id, authority_id) VALUES (2, 2);
INSERT INTO users_authorities(user_id, authority_id) VALUES (2, 6);

INSERT INTO users_authorities(user_id, authority_id) VALUES (3, 3);
INSERT INTO users_authorities(user_id, authority_id) VALUES (3, 7);

INSERT INTO users_authorities(user_id, authority_id) VALUES (4, 9);